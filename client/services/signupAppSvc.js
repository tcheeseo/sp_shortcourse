(function() {
    angular
        .module("SignupApp")
        .service("SignUpAppSVC", SignUpSVC);

    SignUpSVC.$inject = ["$http"];

    function SignUpSVC ($http) {
        var self = this;

        self.handleSignup   = handleSignup;
        self.getCountryList = getCountryList;
        self.getMemberList  = getMemberList;

        function handleSignup(signupData) {
            return ($http.post("/api/members/signUp", {
                    data: signupData
            }));
        };

        function getCountryList() {
            return ($http.get("/api/countries").then(function (response) {
                return response.data;
            }));
        }

        function getMemberList() {
            return ($http.get("/api/members").then(function (response) {
                return response.data;
            }));
        }
    }
})();
