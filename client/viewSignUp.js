(function () {    
    angular
        .module("SignupApp")
        .controller("ViewSignCtrl", ViewSignCtrl);;

    ViewSignCtrl.$inject = ["SignUpAppSVC"];

    function ViewSignCtrl (SignUpSVC) {
        var vm = this;        

        vm.memberList = [];

        vm.getMemberList = getMemberList;

        function getMemberList() {
            SignUpSVC.getMemberList().then(function (resultList) {
                vm.memberList = resultList;
            });
        }
        vm.getMemberList();
    };
})();