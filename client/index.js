(function () {
    angular
        .module("SignupApp")
        .controller("SignupCtrl", SignupCtrl);

    SignupCtrl.$inject = ["SignUpAppSVC"];

    function SignupCtrl(SignUpSVC) {
        var vm = this;

        vm.signupInfo = {};
        vm.countryList = [];       
        vm.signUpMsg = "";

        // expose functions

        vm.initSignupField = initSignupField;
        vm.getCountryList  = getCountryList;
        vm.isValidPassword = isValidPassword;
        vm.isValidContact  = isValidContact;
        vm.isValidAge      = isValidAge;
        vm.isValidSubmit   = isValidSubmit;
        vm.handleSignUp    = handleSignUp;
        vm.showError       = false;

        // init
        initSignupField();
        getCountryList();

        function initSignupField() {
            vm.signupInfo.username = "";
            vm.signupInfo.useremail = "";
            vm.signupInfo.userpassword = "";
            vm.signupInfo.gender = "Male"; 
            vm.signupInfo.dateOfBirth = new Date();
            vm.signupInfo.address = "";
            vm.signupInfo.contact = "";
            vm.signupInfo.country = "";
        }


        function getCountryList () {
            SignUpSVC.getCountryList().then(function (resultList) {
                vm.countryList = resultList;
            });
        }

        function isValidPassword() {
            var regularExpression = /^(?=.*\d)(?=.*[@#$])(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
            return regularExpression.test(vm.signupInfo.userpassword);
        }

        function isValidContact() {
            var regularExpression = /^[0-9 ()+-]+$/;
            return regularExpression.test(vm.signupInfo.contact);
        }

        function isValidAge() {
            var age = new Date().getYear() - vm.signupInfo.dateOfBirth.getYear();
            //console.log("Age : %d", age);
            return (age >= 18);
        }

        function isValidSubmit() {
            vm.showError = true;
            //console.log("Password [%s] vaild %s", vm.signupInfo.userpassword , vm.isValidPassword());
            //console.log("Contact [%s] vaild %s", vm.signupInfo.contact, vm.isValidContact());
            //console.log("Valid Age: " + vm.isValidAge());

            return (vm.isValidPassword() && vm.isValidContact() && vm.isValidAge());
        }

        function handleSignUp() {
            if (vm.isValidSubmit()) {
                SignUpSVC.handleSignup(
                    vm.signupInfo
                ).then(function (response) {
                    initSignupField();
                    vm.signUpMsg = "Registration Success! You will be redirect to the Login Page";
                    vm.showError = false;
                    setTimeout(function(){ window.location.replace("/login.html"); }, 3000);
                });
            }
        }
        


    }

    
})();