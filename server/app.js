var express         = require("express");
var path            = require("path");
var bodyParser      = require('body-parser');

var app = express();

app.use(bodyParser.json());                         // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

require("./routes")(app);

// set port and start webserver
app.set("port", parseInt(process.argv[2]) || parseInt(process.env.APP_PORT) || 3000);
app.listen(app.get("port"), function () {
    console.info("Application started at %s is listening to port %d", new Date(), app.get("port"));
});
