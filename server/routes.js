var express         = require("express");
var path            = require("path");
var mySignupEngine  = require("./signupAppEngine")();

const CLIENT_FOLDER = path.join(__dirname, "../client");
const LIBS_FOLDER   = path.join(CLIENT_FOLDER, "bower_components");

const API_COUNTRIES_URI       = "/api/countries";
const API_MEMBERS_URI         = "/api/members";

module.exports = function (app) {

    app.use(express.static(CLIENT_FOLDER));
    app.use("/libs", express.static(LIBS_FOLDER));


    app.get(API_COUNTRIES_URI, function(req, resp) {
        resp.status(200)
        resp.type("application/json");
        resp.json(mySignupEngine.countryList);
    });

    app.get(API_MEMBERS_URI, function(req, resp) {
        resp.status(200)
        resp.type("application/json");
        resp.json(mySignupEngine.signUpList);
    });

    app.post(API_MEMBERS_URI + "/signUp", function(req, resp) {
        console.log(req.body.data);
        mySignupEngine.addSignUp(req.body.data);

        resp.status(200).end();
    });

    // Catch all
    app.use(function (req, resp) {
        resp.status(440);
        resp.send("Error File not Found");
    });
};